use core::intrinsics;

mod cmath {
    use libc::{c_float, c_double};

    #[link_name = "m"]
    extern "C" {
        pub fn acosf(n: c_float) -> c_float;
        pub fn asinf(n: c_float) -> c_float;
        pub fn atanf(n: c_float) -> c_float;
        pub fn tanf(n: c_float) -> c_float;

        pub fn acos(n: c_double) -> c_double;
        pub fn asin(n: c_double) -> c_double;
        pub fn atan(n: c_double) -> c_double;
        pub fn tan(n: c_double) -> c_double;
    }
}

pub trait Float {
    fn sin(self) -> Self;
    fn cos(self) -> Self;
    fn tan(self) -> Self;

    fn asin(self) -> Self;
    fn acos(self) -> Self;
    fn atan(self) -> Self;
}

impl Float for f32 {
    #[inline]
    fn sin(self) -> f32 {
        unsafe { intrinsics::sinf32(self) }
    }
    #[inline]
    fn cos(self) -> f32 {
        unsafe { intrinsics::cosf32(self) }
    }
    #[inline]
    fn tan(self) -> f32 {
        unsafe { cmath::tanf(self) }
    }

    #[inline]
    fn asin(self) -> f32 {
        unsafe { cmath::asinf(self) }
    }
    #[inline]
    fn acos(self) -> f32 {
        unsafe { cmath::acosf(self) }
    }
    #[inline]
    fn atan(self) -> f32 {
        unsafe { cmath::atanf(self) }
    }
}

impl Float for f64 {
    #[inline]
    fn sin(self) -> f64 {
        unsafe { intrinsics::sinf64(self) }
    }
    #[inline]
    fn cos(self) -> f64 {
        unsafe { intrinsics::cosf64(self) }
    }
    #[inline]
    fn tan(self) -> f64 {
        unsafe { cmath::tan(self) }
    }

    #[inline]
    fn asin(self) -> f64 {
        unsafe { cmath::asin(self) }
    }
    #[inline]
    fn acos(self) -> f64 {
        unsafe { cmath::acos(self) }
    }
    #[inline]
    fn atan(self) -> f64 {
        unsafe { cmath::atan(self) }
    }
}
