use libc::c_char;
use super::ffi;

#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => {{
        use core::fmt::Write;
        let _ = write!(&mut $crate::io::Stdout::new(), $($arg)*);
    }}
}

#[macro_export]
macro_rules! println {
    ($($arg:tt)*) => {{
        use core::fmt::Write;
        let _ = writeln!(&mut $crate::io::Stdout::new(), $($arg)*);
    }}
}

#[macro_export]
macro_rules! die {
    ($code:expr) => {{
        $crate::macros::die($code);
    }};
    ($code:expr, $($arg:tt)*) => {{
        println!($($arg)*);
        $crate::macros::die($code);
    }};
}

#[cfg(unix)]
pub fn die (code: i32) -> ! {
    use libc::exit;
    use alloc::heap_deinit;
    unsafe {
        heap_deinit();
        exit(code);
    }
}

#[macro_export]
macro_rules! cstr {
    ($str:expr) => {
        $crate::macros::str_to_cstr(concat!($str, "\0"))
    }
}

/// ONLY call through cstr!()
/// s MUST be nul-terminated
/// return value only lasts as long as s
pub fn str_to_cstr(s: &str) -> *const c_char {
    extern "rust-intrinsic" {
        fn transmute<T,U>(e: T) -> U;
    }
    struct Repr {
        data: *const c_char,
        _len: usize,
    }
    unsafe {
        transmute::<&str, Repr>(s).data
    }
}

#[macro_export]
macro_rules! rstr {
    ($cstr:expr) => {{
        $crate::macros::cstr_to_str($cstr)
    }}
}

pub unsafe fn cstr_to_str<'a>(cstr: *const c_char) -> &'a str {
    ffi::CStr::from_ptr(cstr).to_str()
}

#[macro_export]
macro_rules! panic {
    ($str: expr) => {
        die!(1, concat!("Panic at {}: {}:\n    ", $str), file!(), line!());
    };
    ($str:expr, $($arg:tt)+) => {
        die!(1, concat!("Panic at {}:{}:\n    ", $str), file!(), line!(), $($arg)+);
    };
}

#[macro_export]
macro_rules! cpanic {
    () => {{
        use $crate::ffi::error::cerror;
        die!(1, "{}", cerror());
    }};
    ($str: expr) => {{
        use $crate::ffi::error::cerror;
        die!(1, concat!($str, ": {}"), cerror());
    }};
}

#[macro_export]
macro_rules! assert {
    ($test: expr) => {
        if !$test {
            panic!(concat!("Assertion failed: ", stringify!($test)));
        }
    };
    ($test: expr, $($arg:tt)+) => {
        if !$test {
            panic!($($arg)+);
        }
    };
}

#[macro_export]
macro_rules! KiB {
    ($kb:expr) => {
        $kb * 1024
    }
}
#[macro_export]
macro_rules! MiB {
    ($mb:expr) => {
        KiB!($mb) * 1024
    }
}
#[macro_export]
macro_rules! GiB {
    ($gb:expr) => {
        MiB!($gb) * 1024
    }
}
