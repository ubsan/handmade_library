#![deprecated = "Test code"]
// TODO: Why is the warning not showing?

pub use self::platform::*;

#[cfg(unix)]
mod platform {
    use core::prelude::*;
    use core::{mem, ops};
    use libc::*;

    use ffi::error::{cerror, Error};
    use ffi::error::Result as CResult;

    pub mod open_flags {
        use libc::{self, c_int};
        pub const APPEND: c_int = libc::O_APPEND;
        pub const TRUNCATE: c_int = libc::O_TRUNC;
        pub const CREATE: c_int = libc::O_CREAT;
        pub const MUST_CREATE: c_int = libc::O_EXCL;
        pub const READ: c_int = libc::O_RDONLY;
        pub const WRITE: c_int = libc::O_WRONLY;
        pub const READWRITE: c_int = libc::O_RDWR;
    }

    pub mod permissions {
        use libc::{self, mode_t};

        pub const READ: mode_t = libc::S_IRUSR;
        pub const WRITE: mode_t = libc::S_IWUSR;
        pub const EXECUTE: mode_t = libc::S_IXUSR;
    }

    pub mod protections {
        use libc::{self, c_int};

        pub const EXEC: c_int = libc::PROT_EXEC;
        pub const READ: c_int = libc::PROT_READ;
        pub const WRITE: c_int = libc::PROT_WRITE;
        pub const NONE: c_int = libc::PROT_NONE;
    }

    #[derive(Clone)] // possible to copy, but you need to explicitly do it
    pub struct File {
        fd: c_int,
    }

    impl File {
        pub fn open(name: *const c_char, flags: c_int, permissions: mode_t) -> CResult<File> {
            use libc::open;
            unsafe {
                match open(name, flags, permissions) {
                    -1 => Err(cerror()),
                    fd => Ok( File { fd: fd } ),
                }
            }
        }

        pub fn create(name: *const c_char, mode: mode_t, append: bool) -> CResult<File> {
            let flags =
                if append {
                    O_CREAT | O_WRONLY | O_APPEND
                } else {
                    O_CREAT | O_WRONLY | O_TRUNC
                };

            unsafe {
                match open(name, flags, mode) {
                    -1 => Err(cerror()),
                    fd => Ok( File { fd: fd } ),
                }
            }
        }

        pub fn read<T>(&self, buff: &mut [T]) -> CResult<Option<usize>> {
            use core::mem::size_of;
            unsafe {
                match read(self.fd, buff.as_mut_ptr() as *mut c_void,
                            (buff.len() * size_of::<T>()) as size_t) {
                    -1 => Err(cerror()),
                    0 => Ok(None), // EOF
                    i => Ok(Some(i as usize)),
                }
            }
        }

        pub fn read_one<T>(&self) -> CResult<Option<T>> {
            use core::mem::{uninitialized, size_of};
            unsafe {
                let mut x: T = uninitialized();
                match read(self.fd, &mut x as *mut T as *mut c_void, size_of::<T>() as size_t) {
                    -1 => Err(cerror()),
                    0 => Ok(None),
                    _ => Ok(Some(x)),
                }
            }
        }

        pub fn write<T>(&mut self, buff: &[T]) -> CResult<()> {
            use core::mem::size_of;
            use libc::{EIO, write};
            unsafe {
                match write(self.fd, buff.as_ptr() as *const c_void,
                            (buff.len() * size_of::<T>()) as size_t) {
                    -1 => Err(cerror()),
                    i if i != (buff.len() * size_of::<T>()) as ssize_t => {
                        println!("{} != {}", i, buff.len());
                        Err( Error { os: EIO, desc: "File not fully written" } )
                    },
                    _ => {
                        match fsync(self.fd) {
                            -1 => Err(cerror()),
                            _ => Ok(()),
                        }
                    }
                }
            }
        }

        pub fn write_one<T>(&mut self, x: &T) -> CResult<()> {
            use core::mem::size_of;
            use libc::{EIO, write};
            unsafe {
                match write(self.fd, x as *const T as *const c_void,
                            size_of::<T>() as size_t) {
                    -1 => Err(cerror()),
                    i if i != size_of::<T>() as ssize_t => {
                        println!("{} != {}", i, size_of::<T>());
                        Err( Error { os: EIO, desc: "File not fully written" } )
                    },
                    _ => {
                        match fsync(self.fd) {
                            -1 => Err(cerror()),
                            _ => Ok(()),
                        }
                    }
                }
            }
        }

        pub fn truncate(&mut self, len: usize) -> CResult<()> {
            use libc::{ftruncate, off_t};
            match unsafe { ftruncate(self.fd, len as off_t) } {
                -1 => Err(cerror()),
                _ => Ok(()),
            }
        }

        pub fn seek(&mut self, off: usize) -> CResult<()> {
            use libc::{lseek, SEEK_SET, off_t};

            match unsafe { lseek(self.fd, off as off_t, SEEK_SET) } {
                -1 => Err(cerror()),
                _ => Ok(()),
            }
        }

        pub fn offset(&self) -> CResult<usize> {
            use libc::{lseek, SEEK_CUR};

            match unsafe { lseek(self.fd, 0, SEEK_CUR) } {
                -1 => Err(cerror()),
                off => Ok(off as usize),
            }
        }

        pub fn len(&self) -> CResult<usize> {
            use libc::{stat, fstat};
            unsafe {
                let mut st: stat = mem::uninitialized();
                match fstat(self.fd, &mut st) {
                    0 => Ok(st.st_size as usize),
                    -1 => Err(cerror()),
                    _ => Err( Error { os: 0, desc: "Unkown" } ),
                }
            }
        }

        pub fn last_write_time(name: *const c_char) -> CResult<time_t> {
            unsafe {
                let mut st: stat = mem::uninitialized();
                match stat(name, &mut st) {
                    0 => Ok(st.st_mtime),
                    _ => Err(cerror()),
                }
            }
        }
    }

    impl Drop for File {
        fn drop(&mut self) {
            unsafe {
                match close(self.fd) {
                    0 => {},
                    -1 => {}, // Error closing file
                    _ => {}, // Shouldn't ever happen
                }
            }
        }
    }

    pub fn create_dir(name: *const c_char) -> CResult<()> {
        use self::permissions::*;
        unsafe {
            let mut st: stat = mem::uninitialized();
            match stat(name, &mut st) {
                -1 =>
                    match mkdir(name, READ | WRITE | EXECUTE) {
                        -1 => Err(cerror()),
                        _ => Ok(()),
                    },
                _ => Ok(()),
            }
        }
    }

    pub struct MemoryMap {
        data: *mut c_char,
        len: usize,
    }

    impl MemoryMap {
        pub fn from_filename(name: *const c_char) -> CResult<MemoryMap> {
            unsafe {
                let mut file = match File::open(name, open_flags::READ, 0) {
                    Ok(f) => f,
                    Err(e) => return Err(e),
                };

                MemoryMap::from_file(&mut file, None, protections::READ)
            }
        }

        pub fn from_file(file: &mut File, len: Option<usize>, prot: c_int) -> CResult<MemoryMap> {
            unsafe {
                let len = match len {
                    Some(l) => {
                        match file.truncate(l) {
                            Ok(_) => {},
                            Err(e) => return Err(e),
                        }
                        l
                    },
                    None => match file.len() {
                        Ok(len) => len,
                        Err(e) => return Err(e),
                    },
                };

                let data = match mmap(0 as *mut c_void, len as size_t,
                                      prot, MAP_SHARED,
                                      file.fd, 0) as usize {
                    -1 => return Err(cerror()),
                    p => p as *mut c_char,
                };

                Ok(MemoryMap {
                    data: data,
                    len: len,
                })
            }
        }

        pub fn write_file(&self, name: *const c_char) -> CResult<()> {
            let mut file = match File::create(name,
                                              permissions::READ | permissions::WRITE,
                                              false) {
                Ok(f) => f,
                Err(e) => return Err(e),
            };

            file.write(&*self)
        }
    }

    impl Drop for MemoryMap {
        fn drop(&mut self) {
            unsafe {
                munmap(self.data as *mut c_void, self.len as size_t);
            }
        }
    }

    impl ops::Deref for MemoryMap {
        type Target = [c_char];

        fn deref(&self) -> &[c_char] {
            use core::slice;
            unsafe {
                slice::from_raw_parts(self.data, self.len)
            }
        }
    }

    impl ops::DerefMut for MemoryMap {
        fn deref_mut(&mut self) -> &mut [c_char] {
            use core::slice;
            unsafe {
                slice::from_raw_parts_mut(self.data, self.len)
            }
        }
    }
}
