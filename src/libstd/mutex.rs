use core::prelude::*;
use core::ops::{Deref, DerefMut};
use core::cell::UnsafeCell;

use alloc::Box;

#[cfg(unix)]
mod platform {
    use core::prelude::*;
    use core::cell::UnsafeCell;

    mod ffi {
        use libc;
        pub use super::os::*;
        extern "C" {
            pub fn pthread_mutex_destroy(lock: *mut pthread_mutex_t) -> libc::c_int;
            pub fn pthread_mutex_lock(lock: *mut pthread_mutex_t) -> libc::c_int;
            pub fn pthread_mutex_trylock(lock: *mut pthread_mutex_t) -> libc::c_int;
            pub fn pthread_mutex_unlock(lock: *mut pthread_mutex_t) -> libc::c_int;
        }
    }

    #[cfg(target_os = "linux")]
    mod os {
        use libc;

        // minus 8 because we have an `align' field
        #[cfg(target_arch = "x86_64")]
        const __SIZEOF_PTHREAD_MUTEX_T: usize = 40 - 8;
        #[cfg(any(target_arch = "x86",
                  target_arch = "arm",
                  target_arch = "mips",
                  target_arch = "mipsel",
                  target_arch = "powerpc"))]
        const __SIZEOF_PTHREAD_MUTEX_T: usize = 24 - 8;
        #[cfg(target_arch = "aarch64")]
        const __SIZEOF_PTHREAD_MUTEX_T: usize = 48 - 8;

        #[cfg(any(target_arch = "x86_64",
                  target_arch = "x86",
                  target_arch = "arm",
                  target_arch = "aarch64",
                  target_arch = "mips",
                  target_arch = "mipsel",
                  target_arch = "powerpc"))]
        const __SIZEOF_PTHREAD_COND_T: usize = 48 - 8;

        #[cfg(any(target_arch = "x86_64",
                  target_arch = "aarch64"))]
        const __SIZEOF_PTHREAD_RWLOCK_T: usize = 56 - 8;

        #[cfg(any(target_arch = "x86",
                  target_arch = "arm",
                  target_arch = "mips",
                  target_arch = "mipsel",
                  target_arch = "powerpc"))]
        const __SIZEOF_PTHREAD_RWLOCK_T: usize = 32 - 8;

        #[repr(C)]
        pub struct pthread_mutex_t {
            __align: libc::c_longlong,
            size: [u8; __SIZEOF_PTHREAD_MUTEX_T],
        }

        pub const PTHREAD_MUTEX_INITIALIZER: pthread_mutex_t = pthread_mutex_t {
            __align: 0,
            size: [0; __SIZEOF_PTHREAD_MUTEX_T],
        };
    }

    pub struct Mutex {
        lock: UnsafeCell<ffi::pthread_mutex_t>,
    }

    pub const MUTEX_INIT: Mutex =
        Mutex{ lock: UnsafeCell { value: ffi::PTHREAD_MUTEX_INITIALIZER } };

    unsafe impl Send for Mutex {}
    unsafe impl Sync for Mutex {}

    impl Mutex {
        #[inline]
        pub unsafe fn lock(&self) {
            let r = ffi::pthread_mutex_lock(self.lock.get());
            debug_assert_eq!(r, 0);
        }
        #[inline]
        pub unsafe fn unlock(&self) {
            let r = ffi::pthread_mutex_unlock(self.lock.get());
            debug_assert_eq!(r, 0);
        }
        #[inline]
        pub unsafe fn try_lock(&self) -> bool {
            ffi::pthread_mutex_trylock(self.lock.get()) == 0
        }
        #[inline]
        #[cfg(not(target_os = "dragonfly"))]
        pub unsafe fn destroy(&self) {
            let r = ffi::pthread_mutex_destroy(self.lock.get());
            debug_assert_eq!(r, 0);
        }
        #[inline]
        #[cfg(target_os = "dragonfly")]
        pub unsafe fn destroy(&self) {
            use libc;
            let r = ffi::pthread_mutex_destroy(self.lock.get());
            debug_assert!(r == 0 || r == libc::EINVAL);
        }
    }
}

pub struct Mutex<T: Send> {
    inner: Box<StaticMutex>,
    data: UnsafeCell<T>,
}

unsafe impl<T: Send> Send for Mutex<T> { }

unsafe impl<T: Send> Sync for Mutex<T> { }

pub struct StaticMutex {
    lock: platform::Mutex,
}

pub struct MutexGuard<'a, T: 'a> {
    __lock: &'a StaticMutex,
    __data: &'a UnsafeCell<T>,
}

impl<'a, T> !Send for MutexGuard<'a, T> { }

/// Static init of a mutex. Can be used to init other mutex consts
pub const MUTEX_INIT: StaticMutex = StaticMutex {
    lock: platform::MUTEX_INIT,
};

impl<T: Send> Mutex<T> {
    /// ALLOCATING FUNCTION: 40 bytes
    pub fn new(x: T) -> Mutex<T> {
        Mutex {
            inner: Box::new(MUTEX_INIT),
            data: UnsafeCell::new(x),
        }
    }

    pub fn lock(&self) -> MutexGuard<T> {
        unsafe { self.inner.lock.lock() }
        MutexGuard::new(&self.inner, &self.data)
    }

    pub fn try_lock(&self) -> Result<MutexGuard<T>, ()> {
        if unsafe { self.inner.lock.try_lock() } {
            Ok(MutexGuard::new(&*self.inner, &self.data))
        } else {
            Err(())
        }
    }
}

#[unsafe_destructor]
impl<T: Send> Drop for Mutex<T> {
    fn drop(&mut self) {
        unsafe { self.inner.lock.destroy() }
    }
}

struct Dummy(UnsafeCell<()>);
unsafe impl Sync for Dummy { }
static DUMMY: Dummy = Dummy(UnsafeCell { value: () });

impl StaticMutex {
    #[inline]
    pub fn lock(&'static self) -> MutexGuard<()> {
        unsafe { self.lock.lock() }
        MutexGuard::new(self, &DUMMY.0)
    }

    #[inline]
    pub fn try_lock(&'static self) -> Result<MutexGuard<()>, ()> {
        if unsafe { self.lock.try_lock() } {
            Ok(MutexGuard::new(self, &DUMMY.0))
        } else {
            Err(())
        }
    }

    pub unsafe fn destroy(&'static self) {
        self.lock.destroy()
    }
}

impl<'mutex, T> MutexGuard<'mutex, T> {
    fn new(lock: &'mutex StaticMutex, data: &'mutex UnsafeCell<T>)
            -> MutexGuard<'mutex, T> {
        MutexGuard {
            __lock: lock,
            __data: data,
        }
    }
}

impl<'mutex, T> Deref for MutexGuard<'mutex, T> {
    type Target = T;

    fn deref<'a>(&'a self) -> &'a T {
        unsafe { &*self.__data.get() }
    }
}

impl<'mutex, T> DerefMut for MutexGuard<'mutex, T> {
    fn deref_mut<'a>(&'a mut self) -> &'a mut T {
        unsafe { &mut *self.__data.get() }
    }
}

#[unsafe_destructor]
impl<'a, T> Drop for MutexGuard<'a, T> {
    #[inline]
    fn drop(&mut self) {
        unsafe {
            self.__lock.lock.unlock();
        }
    }
}

pub fn guard_lock<'a, T>(guard: &MutexGuard<'a, T>) -> &'a platform::Mutex {
    &guard.__lock.lock
}
