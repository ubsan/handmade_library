use core::prelude::*;
use core::ptr::Unique;
use core::mem::size_of;
use core::nonzero::NonZero;
use core::ops;
use core::intrinsics::transmute;

use mutex::{StaticMutex, MUTEX_INIT};
use fs::File;

#[derive(Copy)]
pub struct Heap {
    pub heap: *mut u8,
    pub hptr: *mut u8,
    pub size: isize,
}

static HEAP_MUTEX: StaticMutex = MUTEX_INIT;
static mut HEAP: Heap =
Heap {
    heap: 0 as *mut u8,
    hptr: 0 as *mut u8,
    size: 0,
};

/// Call to setup the heap.
/// HEAP_MUTEX __MUST__ be locked
#[cfg(unix)]
mod platform {
    use super::Heap;

    pub unsafe fn heap_setup(heap: &mut Heap) {
        use libc::*;
        use core::isize;

        let size: size_t = MiB!(256);
        if size as usize > isize::MAX as usize {
            panic!("size in heap_setup is too large!");
        }

        heap.heap = match mmap(0 as *mut c_void, size, PROT_READ | PROT_WRITE,
                               MAP_PRIVATE | MAP_ANON, -1, 0) as isize {
            -1 => {
                cpanic!("Memory allocation error");
            },
            0 => {
                panic!("mmap returned null pointer");
            },
            p => p as *mut u8,
        };

        heap.hptr = heap.heap;
        heap.size = size as isize;
    }

    pub unsafe fn heap_setdown(heap: &mut Heap) {
        use libc::{c_void, size_t};
        use libc::funcs::posix88::mman::munmap;
        unsafe {
            munmap(heap.heap as *mut c_void, heap.size as size_t);
        }
    }
}

/// HEAP_MUTEX __MUST__ be locked
unsafe fn heap_left(heap: &mut Heap) -> usize {
    let heap_end: *mut u8 = heap.heap.offset(heap.size as isize);
    heap_end as usize - heap.hptr as usize
}

pub struct Box<T: ?Sized>(pub Unique<T>);

impl<T: ?Sized> ops::Deref for Box<T> {
    type Target = T;

    fn deref(&self) -> &T { unsafe { transmute(*self.0) } }
}

impl<T: ?Sized> ops::DerefMut for Box<T> {
    fn deref_mut(&mut self) -> &mut T { unsafe { transmute(*self.0) } }
}

impl<T> Box<T> {
    pub fn new(x: T) -> Box<T> {
        use core::ptr::write;
        unsafe {
            let ret = Box::allocate(size_of::<T>());
            write(ret, x);
            transmute(ret)
        }
    }
}

impl<T: ?Sized> Box<T> {
    pub unsafe fn from_raw(raw: *mut T) -> Box<T> {
        Box(Unique::new(raw))
    }

    pub unsafe fn allocate(size: usize) -> *mut T {
        use core::isize;
        assert!(size <= isize::MAX as usize);

        HEAP_MUTEX.lock();
        if HEAP.heap.is_null() {
            platform::heap_setup(&mut HEAP);
        }

        assert!(size <= heap_left(&mut HEAP), "Out of memory");

        let ret = HEAP.hptr;
        HEAP.hptr = HEAP.hptr.offset(size as isize);
        ret as *mut T
    }
}

// Remember that we don't ever free memory in this game
pub struct Rc<T> {
    ptr: NonZero<*mut T>,
}

impl<T> Rc<T> {
    // ALLOCATING FUNCTION: size_of::<T>()
    pub fn new(x: T) -> Rc<T> {
        unsafe {
            Rc {
                ptr: transmute(Box::new(x)),
            }
        }
    }

    // ALLOCATING FUNCTION: size_of::<T>()
    pub fn new_from_sys_heap(x: T) -> Rc<T> {
        use libc::{calloc, size_t};
        use core::ptr::write;
        unsafe {
            let ptr = calloc(1, size_of::<T>() as size_t) as *mut T;
            assert!(!ptr.is_null(), "calloc() failure");
            write(ptr, x);
            Rc {
                ptr: NonZero::new(ptr),
            }
        }
    }
}

unsafe impl<T: Send> Send for Rc<T> { }

unsafe impl<T: Sync> Sync for Rc<T> { }

impl<T> ops::Deref for Rc<T> {
    type Target = T;

    fn deref(&self) -> &T {
        unsafe { transmute(self.ptr) }
    }
}

impl<T> Clone for Rc<T> {
    fn clone(&self) -> Rc<T> {
        Rc { ptr: self.ptr }
    }
}

pub fn write_heap_to_file(file: &mut File) {
    use core::slice::from_raw_parts;
    unsafe {
        HEAP_MUTEX.lock();
        let _ = file.write_one(&HEAP);
        let _ = file.write(from_raw_parts(HEAP.heap, HEAP.size as usize));
    }
}

pub fn read_heap_from_file(file: &File) {
    use core::slice::from_raw_parts_mut;
    unsafe {
        HEAP_MUTEX.lock();
        HEAP = match file.read_one() {
            Ok(Some(h)) => h,
            Ok(None) => {return;},
            Err(_) => {return;},
        };
        let _ = file.read(from_raw_parts_mut(HEAP.heap, HEAP.size as usize));
    }
}

// If you want to initialize the heap w/o allocating anything
pub fn heap_init() {
    unsafe {
        HEAP_MUTEX.lock();
        init(&mut HEAP);
    }
}

unsafe fn init(heap: &mut Heap) {
    if heap.heap.is_null() {
        platform::heap_setup(heap);
    }
}

pub fn heap_deinit() {
    unsafe {
        HEAP_MUTEX.lock();
        platform::heap_setdown(&mut HEAP);
    }
}
