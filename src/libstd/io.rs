pub use self::platform::*;

#[cfg(unix)]
mod platform {
    use core::prelude::*;
    use core::fmt::{Write, Error};
    use libc::{self, c_int, c_void, size_t};
    pub struct FileDesc {
        fd: c_int,
    }

    impl FileDesc {
        pub fn new(fd: c_int) -> FileDesc {
            FileDesc {
                fd: fd
            }
        }

        pub fn write(&self, data: &[u8]) -> Result<usize, ()> {
            let ret = unsafe {
                libc::write(self.fd,
                    data.as_ptr() as *const c_void,
                    data.len() as size_t)
            };

            if ret < 0 {
                Err(())
            } else {
                Ok(ret as usize)
            }
        }
    }

    pub struct Stdout(());

    impl Stdout {
        pub fn new() -> Stdout { Stdout (()) }
    }

    impl Write for Stdout {
        fn write_str(&mut self, s: &str) -> Result<(), Error> {
            match FileDesc::new(libc::STDOUT_FILENO).write(s.as_bytes()) {
                Ok(_) => Ok(()),
                Err(()) => Err(Error),
            }
        }
    }
}
