use prelude::*;

use core::atomic::{AtomicUsize, Ordering};
use core::{marker, slice};
use core::mem::size_of;

// 512 bytes
struct AtomicBuffer<T> {
    pub push_index: AtomicUsize,
    pub pop_index: AtomicUsize,
    pub buff: [u8; 496], // Should be used as a T
    pub _marker: marker::PhantomData<T>,
}

pub struct Sender<T> {
    buffer: *mut AtomicBuffer<T>,
}

unsafe impl<T: Send> Send for Sender<T> { }

impl<T: Copy> Sender<T> {
    pub fn send(&self, x: T) {
        unsafe {
            let ptr = (*self.buffer).buff.as_mut_ptr() as *mut T;
            let len = (*self.buffer).buff.len() / size_of::<T>();
            let buff = slice::from_raw_parts_mut(ptr, len);

            let index = (*self.buffer).push_index.load(Ordering::Relaxed);
            buff[index % len] = x;

            (*self.buffer).push_index.fetch_add(1, Ordering::Relaxed);
            // We may lose channel data, but it's unlikely
            // Remember that this is a single producer, single consumer thing
            // And you should be sending and receiving one at a time
        }
    }

    pub unsafe fn data(&self) -> *mut () {
        self.buffer as *mut ()
    }

    pub unsafe fn from_data(data: *const ()) -> Sender<T> {
        use core::mem::transmute;
        Sender {
            buffer: unsafe { transmute(data) },
        }
    }
}

pub struct Receiver<T> {
    buffer: *mut AtomicBuffer<T>,
}

unsafe impl<T: Send> Send for Receiver<T> { }

impl<T: Copy> Receiver<T> {
    pub fn recv(&self) -> Option<T> {
        unsafe {
            let push_index = (*self.buffer).push_index.load(Ordering::Relaxed);
            let pop_index = (*self.buffer).pop_index.load(Ordering::Relaxed);
            if push_index == pop_index {
                return None;
            }

            let ptr = (*self.buffer).buff.as_mut_ptr() as *mut T;
            let len = (*self.buffer).buff.len() / size_of::<T>();
            let buff = slice::from_raw_parts_mut(ptr, len);

            let ret = buff[pop_index % len];
            (*self.buffer).pop_index.fetch_add(1, Ordering::Relaxed);
            Some(ret)
        }
    }

    pub unsafe fn data(&self) -> *mut () {
        self.buffer as *mut ()
    }

    pub unsafe fn from_data(data: *const ()) -> Receiver<T> {
        use core::mem::transmute;
        Receiver {
            buffer: unsafe { transmute(data) },
        }
    }
}

impl<T: Copy> Iterator for Receiver<T> {
    type Item = T;

    fn next(&mut self) -> Option<T> {
        self.recv()
    }
}

/// ALLOCATING FUNCTION: 512 bytes
pub fn channel<T>() -> (Sender<T>, Receiver<T>) {
    let mut buff = Box::new(AtomicBuffer {
        push_index: AtomicUsize::new(0),
        pop_index: AtomicUsize::new(0),
        buff: [0; 496],
        _marker: marker::PhantomData,
    });

    (Sender { buffer: &mut *buff },
     Receiver { buffer: &mut *buff })
}
