use libc::c_char;

pub struct CStr {
    ptr: *const c_char,
}

impl CStr {
    pub unsafe fn from_ptr(ptr: *const c_char) -> CStr {
        CStr {
            ptr: ptr
        }
    }

    pub unsafe fn to_str<'a>(&self) -> &'a str {
        use core::intrinsics::{transmute, offset};
        use core::slice;

        let mut len = 0;
        loop {
            if *(offset(self.ptr, len as isize)) == 0 {
                break;
            } else {
                len += 1;
            }
        }

        transmute(slice::from_raw_parts(self.ptr, len))
    }
}

pub mod error {
    pub use self::platform::*;
    #[cfg(unix)]
    mod platform {
        use libc::{strerror, c_int};
        use core::prelude;
        use core::fmt;

        pub type Result<T> = prelude::Result<T, Error>;
        pub struct Error {
            pub os: c_int,
            pub desc: &'static str,
        }

        impl fmt::Display for Error {
            fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
                let err_type = os::get_error_name(self.os);
                write!(f, "{}: {}", err_type, self.desc)
            }
        }

        fn to_error(errno: i32) -> Error {
            unsafe {
                Error {
                    os: errno,
                    desc: rstr!(strerror(errno)),
                }
            }
        }

        pub fn cerror() -> Error {
            unsafe {
                to_error(os::errno())
            }
        }

        #[cfg(target_os = "linux")]
        mod os {
            use libc::c_int;
            pub fn get_error_name(errno: c_int) -> &'static str {
                use libc::consts::os::posix88;
                match errno {
                    posix88::E2BIG => "E2BIG",
                    posix88::EACCES => "EACCES",
                    posix88::EADDRINUSE => "EADDRINUSE",
                    posix88::EADDRNOTAVAIL => "EADDRNOTAVAIL",
                    posix88::EAFNOSUPPORT => "EAFNOSUPPORT",
                    posix88::EAGAIN => "EAGAIN",
                    posix88::EALREADY => "EALREADY",
                    posix88::EBADE => "EBADE",
                    posix88::EBADF => "EBADF",
                    posix88::EBADFD => "EBADFD",
                    posix88::EBADMSG => "EBADMSG",
                    posix88::EBADR => "EBADR",
                    posix88::EBADRQC => "EBADRQC",
                    posix88::EBADSLT => "EBADSLT",
                    posix88::EBUSY => "EBUSY",
                    posix88::ECANCELED => "ECANCELED",
                    posix88::ECHILD => "ECHILD",
                    posix88::ECHRNG => "ECHRNG",
                    posix88::ECOMM => "ECOMM",
                    posix88::ECONNABORTED => "ECONNABORTED",
                    posix88::ECONNREFUSED => "ECONNREFUSED",
                    posix88::ECONNRESET => "ECONNRESET",
                    posix88::EDEADLOCK => "EDEADLOCK",
                    posix88::EDESTADDRREQ => "EDESTADDRREQ",
                    posix88::EDOM => "EDOM",
                    posix88::EDQUOT => "EDQUOT",
                    posix88::EEXIST => "EEXIST",
                    posix88::EFAULT => "EFAULT",
                    posix88::EFBIG => "EFBIG",
                    posix88::EHOSTDOWN => "EHOSTDOWN",
                    posix88::EHOSTUNREACH => "EHOSTUNREACH",
                    posix88::EIDRM => "EIDRM",
                    posix88::EILSEQ => "EILSEQ",
                    posix88::EINPROGRESS => "EINPROGRESS",
                    posix88::EINTR => "EINTR",
                    posix88::EINVAL => "EINVAL",
                    posix88::EIO => "EIO",
                    posix88::EISCONN => "EISCONN",
                    posix88::EISDIR => "EISDIR",
                    posix88::EISNAM => "EISNAM",
                    posix88::EKEYEXPIRED => "EKEYEXPIRED",
                    posix88::EKEYREJECTED => "EKEYREJECTED",
                    posix88::EKEYREVOKED => "EKEYREVOKED",
                    posix88::EL2HLT => "EL2HLT",
                    posix88::EL2NSYNC => "EL2NSYNC",
                    posix88::EL3HLT => "EL3HLT",
                    posix88::EL3RST => "EL3RST",
                    posix88::ELIBACC => "ELIBACC",
                    posix88::ELIBBAD => "ELIBBAD",
                    posix88::ELIBMAX => "ELIBMAX",
                    posix88::ELIBSCN => "ELIBSCN",
                    posix88::ELIBEXEC => "ELIBEXEC",
                    posix88::ELOOP => "ELOOP",
                    posix88::EMEDIUMTYPE => "EMEDIUMTYPE",
                    posix88::EMFILE => "EMFILE",
                    posix88::EMLINK => "EMLINK",
                    posix88::EMSGSIZE => "EMSGSIZE",
                    posix88::EMULTIHOP => "EMULTIHOP",
                    posix88::ENAMETOOLONG => "ENAMETOOLONG",
                    posix88::ENETDOWN => "ENETDOWN",
                    posix88::ENETRESET => "ENETRESET",
                    posix88::ENETUNREACH => "ENETUNREACH",
                    posix88::ENFILE => "ENFILE",
                    posix88::ENOBUFS => "ENOBUFS",
                    posix88::ENODATA => "ENODATA",
                    posix88::ENODEV => "ENODEV",
                    posix88::ENOENT => "ENOENT",
                    posix88::ENOEXEC => "ENOEXEC",
                    posix88::ENOKEY => "ENOKEY",
                    posix88::ENOLCK => "ENOLCK",
                    posix88::ENOLINK => "ENOLINK",
                    posix88::ENOMEDIUM => "ENOMEDIUM",
                    posix88::ENOMEM => "ENOMEM",
                    posix88::ENOMSG => "ENOMSG",
                    posix88::ENONET => "ENONET",
                    posix88::ENOPKG => "ENOPKG",
                    posix88::ENOPROTOOPT => "ENOPROTOOPT",
                    posix88::ENOSPC => "ENOSPC",
                    posix88::ENOSR => "ENOSR",
                    posix88::ENOSTR => "ENOSTR",
                    posix88::ENOSYS => "ENOSYS",
                    posix88::ENOTBLK => "ENOTBLK",
                    posix88::ENOTCONN => "ENOTCONN",
                    posix88::ENOTDIR => "ENOTDIR",
                    posix88::ENOTEMPTY => "ENOTEMPTY",
                    posix88::ENOTSOCK => "ENOTSOCK",
                    posix88::ENOTTY => "ENOTTY",
                    posix88::ENOTUNIQ => "ENOTUNIQ",
                    posix88::ENXIO => "ENXIO",
                    posix88::EOPNOTSUPP => "EOPNOTSUPP",
                    posix88::EOVERFLOW => "EOVERFLOW",
                    posix88::EPERM => "EPERM",
                    posix88::EPFNOSUPPORT => "EPFNOSUPPORT",
                    posix88::EPIPE => "EPIPE",
                    posix88::EPROTO => "EPROTO",
                    posix88::EPROTONOSUPPORT => "EPROTONOSUPPORT",
                    posix88::EPROTOTYPE => "EPROTOTYPE",
                    posix88::ERANGE => "ERANGE",
                    posix88::EREMCHG => "EREMCHG",
                    posix88::EREMOTE => "EREMOTE",
                    posix88::EREMOTEIO => "EREMOTEIO",
                    posix88::ERESTART => "ERESTART",
                    posix88::EROFS => "EROFS",
                    posix88::ESHUTDOWN => "ESHUTDOWN",
                    posix88::ESPIPE => "ESPIPE",
                    posix88::ESOCKTNOSUPPORT => "ESOCKTNOSUPPORT",
                    posix88::ESRCH => "ESRCH",
                    posix88::ESTALE => "ESTALE",
                    posix88::ESTRPIPE => "ESTRPIPE",
                    posix88::ETIME => "ETIME",
                    posix88::ETIMEDOUT => "ETIMEDOUT",
                    posix88::ETXTBSY => "ETXTBSY",
                    posix88::EUCLEAN => "EUCLEAN",
                    posix88::EUNATCH => "EUNATCH",
                    posix88::EUSERS => "EUSERS",
                    posix88::EXDEV => "EXDEV",
                    posix88::EXFULL => "EXFULL",
                    _ => "Not a known error",
                }
            }

            pub fn errno() -> c_int {
                extern { fn __errno_location() -> *const c_int; }
                unsafe { *__errno_location() }
            }
        }
    }
}
