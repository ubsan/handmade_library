pub use self::platform::*;

#[cfg(target_os = "linux")]
mod platform {
    use core::mem;
    use libc::c_long;

    pub struct Timer {
        ts: timespec,
    }

    impl Timer {
        pub fn start() -> Timer {
            unsafe {
                let mut time: timespec = mem::uninitialized();
                if clock_gettime(0, &mut time) == -1 {
                    cpanic!()
                } else {
                    Timer { ts: time }
                }
            }
        }

        pub fn flip(&mut self) {
            self.ts = unsafe {
                let mut time: timespec = mem::uninitialized();
                if clock_gettime(0, &mut time) == -1 {
                    cpanic!();
                }
                time
            };
        }

        pub fn since_last(&mut self) -> f32 {
            let time = unsafe {
                let mut time: timespec = mem::uninitialized();
                if clock_gettime(0, &mut time) == -1 {
                    cpanic!()
                }
                time
            };
            Timer::diff_times(self.ts, time)
        }

        pub fn since_last_and_flip(&mut self) -> f32 {
            let time = unsafe {
                let mut time: timespec = mem::uninitialized();
                if clock_gettime(0, &mut time) == -1 {
                    cpanic!();
                }
                time
            };
            let res = Timer::diff_times(self.ts, time);

            self.ts = time;
            res
        }

        fn diff_times(start: timespec, end: timespec) -> f32 {
            let sec = (end.tv_sec - start.tv_sec) as f32;
            let ns = (end.tv_nsec - start.tv_nsec) as f32 / 1_000_000_000.0;
            sec + ns
        }

        pub fn total_time(&self) -> f64 {
            self.ts.tv_sec as f64 +
                (self.ts.tv_nsec as f64 / 1_000_000_000.0)
        }
    }

    #[repr(C)]
    #[derive(Copy)]
    struct timespec {
        tv_sec: c_long, // time_t
        tv_nsec: c_long,
    }

    extern {
        fn clock_gettime(clk_id: u32, ts: *mut timespec) -> i32;
    }
}
