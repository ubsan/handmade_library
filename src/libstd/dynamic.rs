pub use self::platform::*;

// This is mostly taken from rust's DynamicLib std implementation

#[cfg(unix)]
mod platform {
    use core::prelude::*;
    use libc::{c_char, c_int, c_void};

    use definitions::FromVoidPtr;

    use mutex::{StaticMutex, MUTEX_INIT};

    static LOCK: StaticMutex = MUTEX_INIT;

    #[cfg(target_os = "linux")]
    mod os {
        use libc::c_int;

        pub const RTLD_LAZY: c_int = 1;
        pub const RTLD_LOCAL: c_int = 0;
    }

    #[link(name = "dl")]
    extern "C" {
        fn dlopen(filename: *const c_char, flag: c_int) -> *const c_void;
        fn dlsym(handle: *const c_void, name: *const c_char) -> *const c_void;
        fn dlclose(handle: *const c_void) -> c_int;
        fn dlerror() -> *const c_char;
    }

    pub struct DynamicLibrary {
        pub handle: *const c_void,
    }

    impl DynamicLibrary {
        pub fn new(name: *const c_char) -> Result<DynamicLibrary, &'static str> {
            unsafe {
                let _guard = LOCK.lock();
                let _old_error = dlerror();

                let handle = dlopen(name, os::RTLD_LAZY | os::RTLD_LOCAL);

                match dlerror() as usize {
                    0 => Ok(DynamicLibrary {
                        handle: handle,
                    }),
                    p => Err(rstr!(p as *const c_char)),
                }
            }
        }

        /// You must make sure that symbol actually has the correct type
        pub unsafe fn symbol<T: FromVoidPtr>(&self, name: *const c_char) -> Result<T, &'static str> {
            let _guard = LOCK.lock();
            let _old_error = dlerror();

            let res = FromVoidPtr::from_void_ptr(dlsym(self.handle, name) as *const ());

            match dlerror() as usize {
                0 => Ok(res),
                p => Err(rstr!(p as *const c_char)),
            }
        }

        pub fn _drop(&mut self) {
            unsafe { dlclose(self.handle); }
        }
    }

    impl Drop for DynamicLibrary {
        fn drop(&mut self) {
            self._drop()
        }
    }
}
