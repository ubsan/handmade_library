#![crate_type = "lib"]
#![crate_name = "handmade_std"]

#![feature(no_std, core, libc, intrinsics, unsafe_destructor, unique, lang_items)]
#![feature(optin_builtin_traits)]
#![allow(unused_unsafe)]
#![no_std]

#[macro_use] extern crate core;
extern crate libc;
#[macro_use] extern crate definitions;

#[macro_use] pub mod macros;
pub mod io;
pub mod ffi;
pub mod num;
pub mod alloc;
pub mod mutex;
pub mod channel;
pub mod fs;
pub mod timer;
pub mod dynamic;
pub mod pixelbuffer;

pub mod prelude {
    pub use core::prelude::*;
    pub use num::*;
    pub use alloc::{self, Box, Rc};
    pub use fs::{MemoryMap, File};
    pub use timer::Timer;
    pub use dynamic::*;
}
