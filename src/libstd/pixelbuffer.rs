use core::{u32, mem, slice};
use core::mem::size_of;

use alloc::*;
use definitions::PixelBuffer;

// ALLOCATING FUNCTION: width * height * size_of::<u32>()
pub fn new<'a>(width: u32, height: u32) -> PixelBuffer<'a> {
    use core::intrinsics::transmute;

    let pitch = width as usize * mem::size_of::<u32>();

    if pitch > u32::MAX as usize {
        panic!("width * sizeof(u32) is too large");
    }

    let ptr = unsafe {
        Box::allocate(width as usize * pitch)
    };

    if ptr.is_null() {
        panic!("Pixelbuffer allocation failed");
    }

    let buffer: *mut [u32] = unsafe {
            slice::from_raw_parts_mut(ptr, (width * height) as usize)
    };

    let buffer = unsafe { transmute(buffer) };

    PixelBuffer {
        width: width,
        height: height,
        pitch: pitch as u32,
        buffer: buffer,
    }
}
