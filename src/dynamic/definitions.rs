#![crate_type = "lib"]
#![crate_name = "definitions"]

#![feature(no_std, lang_items, core)]
#![no_std]

#[macro_use] extern crate core;

use core::prelude::*;
use core::fmt;

/// This shall ALWAYS be used when defining a function from this file
#[macro_export]
macro_rules! define {
    ($fn_type:ty => $name:ident ($($param:ident : $pt:ty),*) = $body:block) => {
        // TODO: Take this out
        #[allow(private_no_mangle_fns)]
        #[no_mangle]
        pub fn $name ($($param : $pt,)*) {
            if false { let _: $fn_type = $name; } // make sure we're type checking
            $body
        }
    };
    ($fn_type:ty => $name:ident ($($param:ident : $pt:ty),*) -> $ret:ty = $body:block) => {
        // TODO: Take this out
        #[allow(private_no_mangle_fns)]
        #[no_mangle]
        pub fn $name ($($param : $pt,)*) -> $ret {
            if false { let _: $fn_type = $name; } // make sure we're type checking
            $body
        }
    };
}

macro_rules! declare {
    ($type_name:ident => $actual_type:ty) => {
        pub type $type_name = $actual_type;
        impl $crate::FromVoidPtr for $type_name {
            #[inline(always)]
            fn from_void_ptr(ptr: *const ()) -> Self {
                use core::intrinsics::transmute;
                unsafe { transmute(ptr) }
            }
        }
    }
}

pub trait FromVoidPtr {
    fn from_void_ptr(ptr: *const ()) -> Self;
}

impl<T: ?Sized> FromVoidPtr for *const T {
    fn from_void_ptr(ptr: *const ()) -> *const T {
        ptr as *const T
    }
}

// dynamic functions
declare!(UpdateAndRender => fn(&mut GameState, &input::Input, &mut PixelBuffer));
declare!(FillAudio => fn(&mut audio::AudioState, &mut [f32], i32, usize));

// static functions
declare!(AudioCmdSend => fn(*const (), audio::AudioCmd));
declare!(AudioCmdRecv => fn(*const ()) -> Option<audio::AudioCmd>);

pub struct GameState {
    pub offset: (u32, u32),
    pub pc_pos: (u32, u32),
    pub original_hz: f32,
    pub hz: f32,
    pub send: AudioCmdSend,
    pub send_data: *const (),
}

impl fmt::Debug for GameState {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        f.write_fmt(format_args!("offset: ({}, {}),\n
                 pc_pos: ({}, {}),\n
                 original_hz: {},
                 hz: {},
                 send: {:x},
                 send_data: {:x},",
                 self.offset.0, self.offset.1,
                 self.pc_pos.0, self.pc_pos.1,
                 self.original_hz,
                 self.hz,
                 self.send as *const () as usize,
                 self.send_data as usize,
                 ))
    }
}

impl GameState {
    pub fn new(hz: f32, sender: AudioCmdSend, send_data: *const ()) -> GameState {
        GameState {
            offset: (0, 0),
            pc_pos: (0, 0),
            original_hz: hz,
            hz: hz,
            send: sender,
            send_data: send_data,
        }
    }
}

unsafe impl Send for GameState { }

pub struct PixelBuffer<'a> {
    pub buffer: &'a mut [u32],
    pub width: u32,
    pub height: u32,
    pub pitch: u32,
}

pub mod audio {
    use core::prelude::*;

    pub struct AudioState {
        pub hz: f32,
        pub vol: f32,
        pub tsin: f32,
        pub recv: super::AudioCmdRecv,
        pub recv_data: *const (),
    }

    unsafe impl Send for AudioState { }

    #[derive(Copy)]
    pub enum AudioCmd {
        SetHz(f32),
        SetVol(f32),
    }
}

pub mod input {
    use core::prelude::*;
    use core::{ops, slice, mem};

    pub struct Input([Controller; 5]);

    impl Input {
        #[inline]
        pub fn new() -> Input {
            const KEY: Key =
            Key {
                half_transitions: 0,
                down: false,
            };

            const CONTROLLER: Controller =
            Controller {
                connected: false,
                analog: Some((0.0, 0.0)),

                keys: Keys {
                    move_up: KEY,
                    move_down: KEY,
                    move_left: KEY,
                    move_right: KEY,

                    action_up: KEY,
                    action_down: KEY,
                    action_left: KEY,
                    action_right: KEY,

                    shoulder_left: KEY,
                    shoulder_right: KEY,

                    start: KEY,
                    back: KEY,

                    debug_loop: KEY,
                }
            };

            let mut input =
                Input([
                    CONTROLLER.clone(),
                    CONTROLLER.clone(),
                    CONTROLLER.clone(),
                    CONTROLLER.clone(),
                    CONTROLLER.clone()
                ]);
            input[0].connected = true;
            input[0].analog = None;

            input
        }
    }

    impl ops::Deref for Input {
        type Target = [Controller];

        #[inline]
        fn deref(&self) -> &[Controller] {
            &self.0
        }
    }

    impl ops::DerefMut for Input {
        #[inline]
        fn deref_mut(&mut self) -> &mut [Controller] {
            &mut self.0
        }
    }

    impl Clone for Input { // While copying is possible, this thing is way too big for that
        fn clone(&self) -> Input {
            Input([
                self[0].clone(),
                self[1].clone(),
                self[2].clone(),
                self[3].clone(),
                self[4].clone(),
            ])
        }

        fn clone_from(&mut self, source: &Input) {
            *self = source.clone();
        }
    }

    #[derive(Clone)]
    pub struct Controller {
        pub connected: bool,
        pub analog: Option<(f32, f32)>,

        pub keys: Keys,
    }

    impl Controller {
        #[inline]
        pub fn zero(&mut self) {
            for k in self.keys.iter_mut() {
                k.zero()
            }
        }
    }

    impl ops::Deref for Controller {
        type Target = Keys;

        #[inline]
        fn deref(&self) -> &Keys {
            &self.keys
        }
    }

    impl ops::DerefMut for Controller {
        #[inline]
        fn deref_mut(&mut self) -> &mut Keys {
            &mut self.keys
        }
    }

    #[derive(Copy, Clone)]
    pub struct Key {
        pub half_transitions: u32,
        pub down: bool,
    }

    impl Key {
        #[inline]
        pub fn keydown(&mut self) {
            self.half_transitions += 1;
            self.down = true;
        }

        #[inline]
        pub fn keyup(&mut self) {
            self.half_transitions += 1;
            self.down = false;
        }

        // was it pressed this frame?
        pub fn pressed(&self) -> bool {
            self.half_transitions != 0 && self.down
        }

        // was it released this frame?
        pub fn released(&self) -> bool {
            self.half_transitions != 0 && !self.down
        }

        #[inline]
        pub fn zero(&mut self) {
            self.half_transitions = 0;
        }
    }

    #[repr(C)]
    pub enum KeyType {
        MoveUp = 0,
        MoveDown,
        MoveLeft,
        MoveRight,

        ActionUp,
        ActionDown,
        ActionLeft,
        ActionRight,

        ShoulderLeft,
        ShoulderRight,

        Start,
        Back,

        DebugLoop,
    }

    #[derive(Copy, Clone)]
    pub struct Keys {
        // move_up MUST be first, or else you must change ops::Deref
        pub move_up: Key,
        pub move_down: Key,
        pub move_left: Key,
        pub move_right: Key,

        pub action_up: Key,
        pub action_down: Key,
        pub action_left: Key,
        pub action_right: Key,

        pub shoulder_left: Key,
        pub shoulder_right: Key,

        pub start: Key,
        pub back: Key,

        pub debug_loop: Key,
    }

    impl ops::Deref for Keys {
        type Target = [Key];

        fn deref(&self) -> &[Key] {
            unsafe {
                slice::from_raw_parts(&self.move_up as *const Key,
                                      mem::size_of::<Keys>() / mem::size_of::<Key>())
            }
        }
    }

    impl ops::DerefMut for Keys {
        fn deref_mut(&mut self) -> &mut [Key] {
            unsafe {
                slice::from_raw_parts_mut(&mut self.move_up as *mut Key,
                                          mem::size_of::<Keys>() / mem::size_of::<Key>())
            }
        }
    }

    impl ops::Index<KeyType> for Keys {
        type Output = Key;

        fn index<'a>(&'a self, index: KeyType) -> &'a Key {
            use core::ops::Deref;
            &self.deref()[index as usize]
        }
    }

    impl ops::IndexMut<KeyType> for Keys {
        fn index_mut<'a>(&'a mut self, index: KeyType) -> &'a mut Key {
            use core::ops::DerefMut;
            &mut self.deref_mut()[index as usize]
        }
    }
}

#[lang = "stack_exhausted"] extern fn stack_exhausted() {}
#[lang = "eh_personality"] extern fn eh_personality() {}
#[lang = "panic_fmt"]
pub extern fn rust_begin_unwind(_msg: core::fmt::Arguments,
                                _file: &'static str, _line: usize) -> ! {
    loop {}
}
