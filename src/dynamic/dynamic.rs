#![crate_type = "dylib"]
#![crate_name = "dynamic"]

#![feature(no_std, lang_items, core, step_by)]
#![allow(non_upper_case_globals)]
#![no_std]

#[macro_use] extern crate core;
#[macro_use] extern crate definitions;

use core::prelude::*;
use definitions::*;
use definitions::audio::*;

fn weird_gradient_pattern(buff: &mut PixelBuffer, offset: (u32, u32)) {
    let mut pixel: usize = 0;
    for y in 0..buff.height {
        for x in 0..buff.width {
            let green = (x.wrapping_sub(offset.0)) as u8;
            let blue = (y.wrapping_sub(offset.1)) as u8;

            buff.buffer[pixel] = ((green  as u32) << 8) | ((blue as u32) << 0);
            pixel += 1;
        }
    }
}

fn render_player(buffer: &mut PixelBuffer, position: (u32, u32)) {
    use core::mem::size_of;
    for y in position.1..(position.1 + 10) {
        for x in position.0..(position.0 + 10) {
            if y < buffer.height && x < buffer.width {
                let pixel = ((y * buffer.pitch) as usize / size_of::<u32>()) + x as usize;
                buffer.buffer[pixel as usize] = 0xFFFFFFFF;
            }
        }
    }
}

fn direction(input: &input::Input) -> (f32, f32) {
    let (mut x, mut y) = (0.0, 0.0);
    for c in input.iter() {
        if c.connected {
            if c.analog.is_some() {
                // TODO: implement analog controllers
            } else {
                let keys = c.keys;

                if keys.move_up.down {
                    y -= 1.0;
                }
                if keys.move_down.down {
                    y += 1.0;
                }
                if keys.move_left.down {
                    x -= 1.0;
                }
                if keys.move_right.down {
                    x += 1.0;
                }
            }
        }
    }

    (x, y)
}

fn pc_direction(input: &input::Input) -> (u32, u32) {
    let (mut x, mut y) = (0, 0);
    for c in input.iter() {
        if c.connected {
            if c.analog.is_some() {
                // TODO: implement analog controllers
            } else {
                let keys = c.keys;

                if keys.move_up.down {
                    y -= 1;
                }
                if keys.move_down.down {
                    y += 1;
                }
                if keys.move_left.down {
                    x -= 1;
                }
                if keys.move_right.down {
                    x += 1;
                }
                if keys.action_up.pressed() {
                    y -= 5;
                }
            }
        }
    }

    (x, y)
}

define!{UpdateAndRender =>
update_and_render(game_state: &mut GameState, input: &input::Input,
                  buffer: &mut PixelBuffer) = {
    let dir = direction(input);
    game_state.offset.0 += dir.0 as u32;
    game_state.offset.1 += dir.1 as u32;
    let pc_dir = pc_direction(input);
    game_state.pc_pos.0 += (pc_dir.0 * 4) as u32;
    game_state.pc_pos.1 += (pc_dir.1 * 4) as u32;

    let hz = game_state.original_hz + (dir.0 + dir.1) * 50.0;
    if hz != game_state.hz {
        (game_state.send)(game_state.send_data, AudioCmd::SetHz(hz));
        game_state.hz = hz;
    }

    weird_gradient_pattern(buffer, game_state.offset);
    render_player(buffer, game_state.pc_pos);
}}

define!{FillAudio =>
fill_audio(audio_state: &mut audio::AudioState, buff: &mut [f32], freq: i32,
           channels: usize) = {
    use core::f32::consts::PI;
    use core::intrinsics;

    match (audio_state.recv)(audio_state.recv_data) {
        Some(AudioCmd::SetHz(hz)) => audio_state.hz = hz,
        Some(AudioCmd::SetVol(vol)) => audio_state.vol = vol,
        None => {},
    }

    let period = freq as f32 / audio_state.hz * 2.0;

    for i in (0..buff.len()).step_by(channels) {
        audio_state.tsin += 2.0 * PI / period;
        for j in 0..channels {
            buff[i+j] = unsafe { intrinsics::sinf32(audio_state.tsin) } * audio_state.vol;
        }
    }

    while audio_state.tsin > 2.0 * PI {
        audio_state.tsin -= 2.0 * PI;
    }
}}
