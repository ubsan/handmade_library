#![crate_type = "bin"]
#![crate_name = "handmade_hero"]

#![feature(no_std, core, start, lang_items)]
#![no_std]

#[macro_use] extern crate core;
#[macro_use] extern crate handmade_std as std;
extern crate sdl2_sys;
extern crate handmade_sdl as sdl;
#[macro_use] extern crate definitions;

mod audio;

use std::prelude::*;
use std::mutex::Mutex;

use sdl::gameloop::GameLoop;
use sdl::graphics::FrameRate;

use definitions::{UpdateAndRender, GameState, FillAudio};

use audio::audio_cmd_sender;

macro_rules! dynamic_libfile {
    () => { cstr!("obj/libdynamic.so") };
}

macro_rules! dynamic_libfile_gate {
    () => { cstr!("obj/.libdynamic.so.gate") };
}

macro_rules! loop_file {
    () => { cstr!("assets/loop.hmh") };
}

struct DynamicFunctions {
    library: DynamicLibrary,
    timestamp: isize,
    pub update_and_render: UpdateAndRender,
    pub fill_audio: Rc<Mutex<FillAudio>>,
}

impl DynamicFunctions {
    // ALLOCATING FUNCTION: 48 bytes
    pub fn new() -> DynamicFunctions {
        let library =
            match DynamicLibrary::new(dynamic_libfile!()) {
                Ok(h) => h,
                Err(e) => panic!("{}", e),
        };

        let update_and_render: UpdateAndRender =
            unsafe { match library.symbol(cstr!("update_and_render")) {
                Ok(p) => p,
                Err(e) => panic!("{}", e),
            }
        };

        // Allocates 48 bytes, from the system heap (i.e. calloc)
        // This is so that it isn't reloaded with the heap load
        let fill_audio: Rc<Mutex<FillAudio>> = Rc::new_from_sys_heap(Mutex::new(
            unsafe { match library.symbol(cstr!("fill_audio")) {
                Ok(p) => p,
                Err(e) => panic!("{}", e),
            }}
        ));

        let timestamp = match File::last_write_time(dynamic_libfile_gate!()) {
            Ok(t) => t as isize,
            Err(e) => panic!("{}", e),
        };

        DynamicFunctions {
            library: library,
            timestamp: timestamp,
            update_and_render: update_and_render,
            fill_audio: fill_audio,
        }
    }

    pub fn reload(mut self) -> DynamicFunctions {
        use core::mem::drop;
        let timestamp = match File::last_write_time(dynamic_libfile_gate!()) {
            Ok(t) => t as isize,
            Err(_) => self.timestamp, // TODO: Logging
        };

        if timestamp > self.timestamp {
            self.timestamp = timestamp;

            let mut fill_audio_mutex = self.fill_audio.lock();
            drop(self.library);
            self.library =
                match DynamicLibrary::new(dynamic_libfile!()) {
                    Ok(h) => h,
                    Err(e) => panic!("{}", e),
            };

            self.update_and_render =
                unsafe { match self.library.symbol(cstr!("update_and_render")) {
                    Ok(p) => p,
                    Err(e) => panic!("{}", e),
                }
            };

            *fill_audio_mutex =
                unsafe { match self.library.symbol(cstr!("fill_audio")) {
                    Ok(p) => p,
                    Err(e) => panic!("{}", e),
                }
            };
        }

        self
    }
}

fn main() -> i32 {
    let mut dyn = DynamicFunctions::new();

    let hz = 463.2; // high c
    // Allocates 544 bytes
    let (audio_func, audio_sender) = audio::Audio::new(dyn.fill_audio.clone(), hz, 0.0);
    // Allocates width * height * size_of::<u32>() bytes (1228800 bytes)
    let mut hh = sdl::init(640, 480, audio_func);

    let mut game_state = Box::new(GameState::new(hz, audio_cmd_sender,
                                        unsafe { audio_sender.data() }));

    hh.audio.play();

    let mut frame_rate = FrameRate::start(1.0 / (FrameRate::display_hz() / 2) as f32);

    for e in GameLoop::new(loop_file!()) {
        dyn = dyn.reload();

        (dyn.update_and_render)(&mut game_state, e, &mut hh.pixels);
        hh.renderer.blit(&hh.pixels);

        frame_rate.wait();
    }

    0
}

#[start]
fn start(_argc: isize, _argv: *const *const u8) -> isize {
    let code = main();
    die!(code);
}

// Necessary because rust bug
#[allow(non_snake_case)]
#[no_mangle]
pub extern fn _Unwind_Resume() {
    println!("Deader");
    die!(1);
}
