use std::prelude::*;
use std::channel::{Sender, Receiver, channel};

use sdl::audio::AudioCallback;

use definitions::audio::*;
use definitions::{FillAudio, AudioCmdSend, AudioCmdRecv};

use std::mutex::Mutex;

pub struct Audio {
    func: Rc<Mutex<FillAudio>>,
    state: Box<AudioState>,
}

impl Audio {
    /// ALLOCATING FUNCTION: 544 bytes
    pub fn new(func: Rc<Mutex<FillAudio>>, hz: f32, volume: f32) ->
              (Audio, Sender<AudioCmd>) {
        let (send, recv) = channel();
        (Audio {
            func: func,
            state: Box::new(AudioState {
                hz: hz,
                vol: volume,
                tsin: 0.0,
                recv: audio_cmd_receiver,
                recv_data: unsafe { recv.data() },
            })
        }, send)
    }
}

define!{AudioCmdSend =>
audio_cmd_sender(data: *const (), x: AudioCmd) = {
    unsafe { Sender::from_data(data).send(x) };
}}

define!{AudioCmdRecv =>
audio_cmd_receiver(data: *const ()) -> Option<AudioCmd> = {
    unsafe { Receiver::from_data(data).recv() }
}}

impl AudioCallback for Audio {
    fn callback(&mut self, buff: &mut [f32], freq: i32, channels: u8) {
        self.func.lock()(&mut self.state, buff, freq, channels as usize);
    }
}
