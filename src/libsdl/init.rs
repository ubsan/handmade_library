use std::prelude::*;
use std::pixelbuffer;

use definitions::PixelBuffer;

use sdl::sdl::{SDL_Init, SDL_INIT_EVERYTHING, SDL_GetError, SDL_Quit};

use render::Renderer;
use audio::{AudioCallback, AudioDevice};

pub struct HandmadeHero<'a, CB: AudioCallback> {
    pub renderer: Renderer,
    pub pixels: PixelBuffer<'a>,
    pub audio: AudioDevice<CB>,
}

/// ALLOCATING FUNCTION: width * height * size_of::<u32>() bytes
pub fn init<'a, CB: AudioCallback>(width: u32, height: u32, func: CB) -> HandmadeHero<'a, CB> {
    unsafe {
        if SDL_Init(SDL_INIT_EVERYTHING) < 0 {
            panic!("{}", rstr!(SDL_GetError()));
        }

        let renderer = Renderer::new(width as i32, height as i32);

        let pixels = pixelbuffer::new(width, height);

        let freq = 44100;
        let channels = 2;
        let audio = AudioDevice::new(func, freq, channels);

        HandmadeHero {
            renderer: renderer,
            pixels: pixels,
            audio: audio,
        }
    }
}

#[unsafe_destructor]
impl<'a, CB: AudioCallback> Drop for HandmadeHero<'a, CB> {
    fn drop(&mut self) {
        unsafe {
            SDL_Quit();
        }
    }
}
