use std::prelude::*;

use core::ptr;

use libc::{c_int, uint32_t};

use sdl::sdl::SDL_GetError;
use sdl::video::{SDL_WindowFlags, SDL_Window, SDL_DestroyWindow};
use sdl::render::*;
use sdl::pixels::SDL_PIXELFORMAT_ARGB8888;

use definitions::PixelBuffer;

pub struct Renderer {
    window: *const SDL_Window,
    renderer: *const SDL_Renderer,
    texture: *const SDL_Texture,
}

impl Renderer {
    pub fn new(width: i32, height: i32) -> Renderer {
        let window: *const SDL_Window = ptr::null();
        let renderer: *const SDL_Renderer = ptr::null();

        let flags =
            SDL_WindowFlags::SDL_WINDOW_SHOWN as u32 |
            SDL_WindowFlags::SDL_WINDOW_RESIZABLE as u32;


        let texture = unsafe {
            if SDL_CreateWindowAndRenderer(width as c_int, height as c_int,
                    flags as uint32_t, &window, &renderer) != 0 {
                panic!("{}", rstr!(SDL_GetError()));
            }

            let texture = SDL_CreateTexture(renderer,
                SDL_PIXELFORMAT_ARGB8888 as uint32_t,
                SDL_TEXTUREACCESS_STREAMING as c_int,
                width as c_int, height as c_int);

            if texture == ptr::null() {
                panic!("{}", rstr!(SDL_GetError()));
            } else {
                texture
            }
        };

        Renderer {
            window: window,
            renderer: renderer,
            texture: texture,
        }
    }

    pub fn blit(&mut self, buffer: &PixelBuffer) {
        use sdl::render::{SDL_UpdateTexture, SDL_RenderClear, SDL_RenderCopy,
            SDL_RenderPresent};

        unsafe {
            if SDL_UpdateTexture(self.texture, ptr::null(),
                    buffer.buffer.as_ptr() as *const _,
                    buffer.pitch as c_int) != 0 {
                // TODO: Logging
            }

            if SDL_RenderClear(self.renderer) != 0 {
                // TODO: Logging
            }

            if SDL_RenderCopy(self.renderer, self.texture, ptr::null(),
                    ptr::null()) != 0 {
                // TODO: Logging
            }

            SDL_RenderPresent(self.renderer);
        }
    }
}

impl Drop for Renderer {
    fn drop(&mut self) {
        unsafe {
            SDL_DestroyTexture(self.texture);
            SDL_DestroyRenderer(self.renderer);
            SDL_DestroyWindow(self.window);
        }
    }
}
