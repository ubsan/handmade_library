use std::prelude::*;

pub struct FrameRate {
    sec_per_frame: f32,
    timer: Timer,
}

impl FrameRate {
    pub fn start(sec_per_frame: f32) -> FrameRate {
        FrameRate {
            sec_per_frame: sec_per_frame,
            timer: Timer::start(),
        }
    }

    pub fn wait(&mut self) {
        let mut since_last = self.timer.since_last();

        if since_last > self.sec_per_frame {
            // TODO: Logging
            self.timer.flip();
            return;
        }

        while since_last < self.sec_per_frame {
            since_last = self.timer.since_last();
        }
        self.timer.flip();
    }

    pub fn display_hz() -> i32 {
        unsafe {
            use sdl::sdl::SDL_GetError;
            use sdl::video::{SDL_DisplayMode, SDL_GetDisplayMode};
            use sdl::pixels::SDL_PIXELFORMAT_UNKNOWN;
            use core::ptr;

            let mode = SDL_DisplayMode {
                format: SDL_PIXELFORMAT_UNKNOWN,
                w: 0,
                h: 0,
                refresh_rate: 0,
                driverdata: ptr::null(),
            };

            if SDL_GetDisplayMode(0, 0, &mode) != 0 {
                panic!("{}", rstr!(SDL_GetError()));
            }

            mode.refresh_rate
        }
    }
}
