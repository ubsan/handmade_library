#![crate_type = "lib"]
#![crate_name = "handmade_sdl"]

#![feature(no_std, core, libc, unsafe_destructor)]
#![no_std]
#[macro_use] extern crate core;
extern crate libc;
#[macro_use] extern crate handmade_std as std;
extern crate sdl2_sys as sdl;
extern crate definitions;

mod init;
pub use init::*;

pub mod audio;
pub mod render;
pub mod gameloop;
pub mod input;
pub mod graphics;
