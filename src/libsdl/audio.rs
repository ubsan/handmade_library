use std::prelude::*;

mod ffi {
    use std::prelude::*;
    use core::intrinsics::transmute;
    use libc::{c_void, c_int, uint8_t};

    use sdl::audio::*;
    use sdl::sdl::SDL_GetError;

    use super::{AudioCallback, AudioDevice};

    pub extern "C" fn audio_callback_marshall<CB: AudioCallback> (userdata: *const c_void,
                                                                  stream: *const uint8_t,
                                                                  len: c_int) {
        use core::raw::Slice;
        use core::mem::size_of;
        unsafe {
            let cb_userdata: &mut AudioCallbackData<CB> = transmute(userdata);
            let buff: &mut [f32] = transmute(Slice {
                data: stream,
                len: len as usize / size_of::<f32>(),
            });

            cb_userdata.callback.callback(buff, cb_userdata.freq,
                cb_userdata.channels);
        }
    }

    pub struct AudioSpecDesired<CB: AudioCallback> {
        pub freq: i32,
        pub channels: u8,
        pub samples: u16,
        pub callback: CB,
    }

    impl<CB: AudioCallback> AudioSpecDesired<CB> {
        fn convert_to_raw(freq: i32, channels: u8, samples: u16,
                          userdata: *mut AudioCallbackData<CB>) -> SDL_AudioSpec {
            unsafe {
                SDL_AudioSpec {
                    freq: freq,
                    format: AUDIO_F32,
                    channels: channels,
                    silence: 0,
                    samples: samples,
                    padding: 0,
                    size: 0,
                    callback: Some(transmute(audio_callback_marshall::<CB>)),
                    userdata: userdata as *const _,
                }
            }
        }

        /// I don't know why, but if I use the default Box, SDL or something screws
        /// up. Currently, I'm just mmaping it, because it does end up working.
        fn callback_to_userdata(callback: CB, freq: i32, channels: u8)
                -> *mut AudioCallbackData<CB> {
            use core::ptr::null;
            use core::mem::size_of;
            use libc::{c_void, size_t};
            use libc::funcs::posix88::mman::mmap;
            unsafe {
                // PROT_READ | PROT_WRITE
                // MAP_PRIVATE | MAP_ANON
                let userdata = mmap(null::<c_void>() as *mut c_void,
                    size_of::<AudioCallbackData<CB>>() as size_t, 2, 34, -1, 0)
                    as *mut AudioCallbackData<CB>;

                (*userdata).callback = callback;
                (*userdata).freq = freq;
                (*userdata).channels = channels;

                userdata
            }
        }

        pub fn open_audio_device(self) -> AudioDevice<CB> {
            use core::mem::uninitialized;
            use core::ptr::null;

            let userdata = AudioSpecDesired::callback_to_userdata(self.callback,
                self.freq, self.channels);

            let desired = AudioSpecDesired::convert_to_raw(self.freq, self.channels,
                self.samples, userdata);

            let mut obtained = unsafe { uninitialized::<SDL_AudioSpec>() };

            unsafe {
                let dev_id = SDL_OpenAudioDevice(null(), 0, &desired, &mut obtained, 0);
                match dev_id {
                    0 => {
                        panic!("{}", rstr!(SDL_GetError()))
                    },
                    id => {
                        let spec = AudioSpec::convert_from_raw(obtained);

                        AudioDevice {
                            id: DeviceID(id),
                            _spec: spec,
                            _userdata: AudioCallbackDataWrapper(userdata),
                        }
                    }
                }
            }
        }
    }

    pub struct AudioSpec {
        pub freq: i32,
        pub format: SDL_AudioFormat,
        pub channels: u8,
        pub silence: u8,
        pub samples: u16,
        pub size: u32,
    }

    impl AudioSpec {
        fn convert_from_raw(spec: SDL_AudioSpec) -> AudioSpec {
            AudioSpec {
                freq: spec.freq,
                format: spec.format,
                channels: spec.channels,
                silence: spec.silence,
                samples: spec.samples,
                size: spec.size,
            }
        }
    }

    pub struct DeviceID(SDL_AudioDeviceID);

    impl DeviceID {
        pub fn play(&self) {
            unsafe { SDL_PauseAudioDevice(self.0, 0) }
        }

        pub fn pause(&self) {
            unsafe { SDL_PauseAudioDevice(self.0, 1) }
        }
    }

    impl Drop for DeviceID {
        fn drop(&mut self) {
            unsafe { SDL_CloseAudioDevice(self.0) }
        }
    }

    pub struct AudioCallbackDataWrapper<CB>(*mut AudioCallbackData<CB>);

    #[unsafe_destructor]
    impl<CB> Drop for AudioCallbackDataWrapper<CB> {
        fn drop(&mut self) {
            use core::mem::size_of;
            use libc::funcs::posix88::mman::munmap;
            use libc::{c_void, size_t};
            unsafe {
                munmap(self.0 as *mut c_void,
                    size_of::<AudioCallbackData<CB>>() as size_t);
            }
        }
    }

    pub struct AudioCallbackData<CB> {
        callback: CB,
        freq: i32,
        channels: u8,
    }
}

pub trait AudioCallback: Send {
    fn callback(&mut self, buff: &mut [f32], freq: i32, channels: u8);
}

pub struct AudioDevice<CB: AudioCallback> {
    id: ffi::DeviceID,
    _spec: ffi::AudioSpec,
    _userdata: ffi::AudioCallbackDataWrapper<CB>,
}

impl<CB: AudioCallback> AudioDevice<CB> {
    pub fn new(callback: CB, freq: i32, channels: u8) -> AudioDevice<CB> {
        ffi::AudioSpecDesired {
            freq: freq,
            channels: channels,
            samples: 0,
            callback: callback,
        }.open_audio_device()
    }

    pub fn play(&self) {
        self.id.play();
    }

    pub fn pause(&self) {
        self.id.pause();
    }
}
