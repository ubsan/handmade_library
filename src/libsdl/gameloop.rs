use std::prelude::*;
use core::{marker, mem};

use definitions::input::{Input, KeyType};
use input::SdlController;

use libc::c_char;

#[derive(Copy, PartialEq, Eq)]
enum LoopState {
    Normal,
    Recording,
    Playing,
}

pub struct GameLoop<'a> {
    input: Input,
    loop_state: LoopState,
    loop_filename: *const c_char,
    loop_file: Option<File>,
    _marker: marker::PhantomData<&'a mut ()>,
}

impl<'a> GameLoop<'a> {
    pub fn new(loop_file: *const c_char) -> GameLoop<'a> {
        GameLoop {
            input: Input::new(),
            loop_state: LoopState::Normal,
            loop_filename: loop_file,
            loop_file: None,
            _marker: marker::PhantomData,
        }
    }

    fn normal(&mut self) -> bool {
        use sdl::event::*;
        use sdl::scancode::*;

        let mut e = unsafe { mem::uninitialized() };
        let mut quit = false;

        for c in self.input.iter_mut() {
            c.zero()
        }

        while unsafe { SDL_PollEvent(&mut e) == 1 } {
            let event_type = unsafe {*e._type()};
            match event_type {
                SDL_KEYDOWN => {
                    let e = unsafe{*e.key()};
                    if e.repeat == 0 {
                        if let Err(s) = self.input[0].keyboard_down(e.keysym.scancode) {
                            match s {
                                SDL_SCANCODE_ESCAPE | SDL_SCANCODE_Q => quit = true,
                                _ => {},
                            }
                        }
                    }
                },
                SDL_KEYUP => {
                    let e = unsafe{*e.key()};
                    let _ = self.input[0].keyboard_up(e.keysym.scancode);
                },
                SDL_QUIT => quit = true,
                _ => {},
            }
        }

        if self.input[0][KeyType::DebugLoop].pressed() {
            if self.loop_state == LoopState::Normal {
                self.start_recording();
                self.loop_state = LoopState::Recording;
            }
        }

        quit
    }

    fn recording(&mut self) -> bool {
        let quit = self.normal();
        self.record_input();

        if self.input[0][KeyType::DebugLoop].pressed() {
            self.stop_recording();
            self.start_playing();
            self.loop_state = LoopState::Playing;
        }

        quit
    }

    fn playing(&mut self) -> bool {
        use sdl::event::*;
        use sdl::scancode::*;
        let should_reset = match self.loop_file {
            Some(ref f) => match f.read_one() {
                Ok(Some(i)) => {
                    self.input = i;
                    false
                },
                Ok(None) => true,
                Err(e) => panic!("{}", e),
            },
            None => panic!("loop_file is not open for reading"),
        };

        if should_reset {
            self.stop_playing();
            self.start_playing();
        }

        let mut e = unsafe { mem::uninitialized() };
        let mut quit = false;

        while unsafe { SDL_PollEvent(&mut e) == 1 } {
            let event_type = unsafe { *e._type() };
            match event_type {
                SDL_KEYDOWN => {
                    let e = unsafe { *e.key() };
                    if e.repeat == 0 {
                        if e.keysym.scancode == SDL_SCANCODE_ESCAPE ||
                                e.keysym.scancode == SDL_SCANCODE_Q {
                            quit = true;
                        }
                    }
                },
                SDL_QUIT => quit = true,
                _ => {},
            }
        }

        quit
    }

    fn start_recording(&mut self) {
        use std::fs::open_flags as flag;
        use std::fs::permissions as perm;
        self.loop_file =
            match File::open(self.loop_filename,
                             flag::WRITE | flag::TRUNCATE | flag::CREATE,
                             perm::READ | perm::WRITE) {
                Ok(mut f) => {
                    alloc::write_heap_to_file(&mut f);
                    Some(f)
                },
                Err(e) => panic!("{}", e),
            };
    }

    fn stop_recording(&mut self) {
        self.loop_file = None;
    }

    fn record_input(&mut self) {
        match self.loop_file {
            Some(ref mut f) => {
                match f.write_one(&self.input) {
                    Ok(()) => {},
                    Err(e) => panic!("{}", e),
                }
            },
            None => panic!("record_input() was called without first opening a file"),
        }
    }

    fn start_playing(&mut self) {
        use std::fs::open_flags as flag;
        self.loop_file = match File::open(self.loop_filename, flag::READ, 0) {
            Ok(f) => {
                alloc::read_heap_from_file(&f);
                Some(f)
            },
            Err(e) => panic!("{}", e),
        };
    }

    fn stop_playing(&mut self) {
        self.loop_file = None;
    }
}

impl<'a> Iterator for GameLoop<'a> {
    type Item = &'a Input;

    fn next(&mut self) -> Option<&'a Input> {
        use core::mem::transmute;

        let iter_type = self.loop_state;
        let quit = match iter_type {
            LoopState::Normal => self.normal(),
            LoopState::Recording => self.recording(),
            LoopState::Playing => self.playing(),
        };

        if quit {
            None
        } else {
            Some( unsafe { transmute(&self.input) } )
        }
    }
}
