use core::prelude::*;

use sdl::scancode::*;

use definitions::input::{Key, Controller, KeyType};

pub trait SdlController {
     fn keyboard_down(&mut self, scan: SDL_Scancode) -> Result<(), SDL_Scancode>;
     fn keyboard_up(&mut self, scan: SDL_Scancode) -> Result<(), SDL_Scancode>;
     fn keystate(&self, scan: SDL_Scancode) -> Result<Key, ()>;
}

impl SdlController for Controller {
    #[inline]
     fn keyboard_down(&mut self, scan: SDL_Scancode) -> Result<(), SDL_Scancode> {
        match index_from_scancode(scan) {
            Ok(i) => {
                self.keys[i].keydown();
                Ok(())
            },
            Err(s) => Err(s),
        }
    }

    #[inline]
     fn keyboard_up(&mut self, scan: SDL_Scancode) -> Result<(), SDL_Scancode> {
        match index_from_scancode(scan) {
            Ok(i) => {
                self.keys[i].keyup();
                Ok(())
            },
            Err(s) => Err(s),
        }
    }

    #[inline]
     fn keystate(&self, scan: SDL_Scancode) -> Result<Key, ()> {
        match index_from_scancode(scan) {
            Ok(i) => Ok(self.keys[i]),
            Err(_) => Err(()),
        }
    }
}

fn index_from_scancode(scan: SDL_Scancode) -> Result<KeyType, SDL_Scancode> {
    match scan {
        SDL_SCANCODE_UP => Ok(KeyType::MoveUp),
        SDL_SCANCODE_W => Ok(KeyType::ActionUp),

        SDL_SCANCODE_DOWN => Ok(KeyType::MoveDown),
        SDL_SCANCODE_S => Ok(KeyType::ActionDown),

        SDL_SCANCODE_LEFT => Ok(KeyType::MoveLeft),
        SDL_SCANCODE_A => Ok(KeyType::ActionLeft),

        SDL_SCANCODE_RIGHT => Ok(KeyType::MoveRight),
        SDL_SCANCODE_D => Ok(KeyType::ActionRight),

        SDL_SCANCODE_Q => Ok(KeyType::ShoulderLeft),
        SDL_SCANCODE_E => Ok(KeyType::ShoulderRight),

        SDL_SCANCODE_RETURN => Ok(KeyType::Start),
        SDL_SCANCODE_LSHIFT => Ok(KeyType::Back),

        SDL_SCANCODE_L => Ok(KeyType::DebugLoop),

        s => Err(s),
    }
}
