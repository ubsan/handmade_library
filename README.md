Handmade Hero Rust/SDL
======================

This is an implementation of Casey Muratori's Handmade Hero in Rust, based on SDL, and using
`#![no_std]`

I don't know if this is a legal thing, because it's a reimplementation. I think, legally, it would
be a clone? Because it's in a different language, and completely differently made? Contact me, if
you think I'm infringing.

If you'd like to contribute, please do! I currently need someone to do controller code, because I
don't have any xbox controllers, and I need someone to implement platform specific code on
Windows/Mac OS X.

I'm currently working on buying an xbox controller, but I don't have the money to do so. Next
payday, and I will implement controller support!

Version: 0.0.22
