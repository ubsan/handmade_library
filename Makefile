RC = rustc
RFLAGS = -O

all: _definitions _libstd _libsdl _engine _dynamic

debug: RFLAGS = -g
debug: all

release: all

_engine: _definitions obj
	$(RC) $(RFLAGS) src/main.rs -L obj -C link-args=-lpthread

_libstd: _definitions obj
	$(RC) $(RFLAGS) src/libstd/lib.rs -L obj --out-dir obj

_libsdl: _libsdl_sys _definitions obj
	$(RC) $(RFLAGS) src/libsdl/lib.rs -L obj --out-dir obj

_libsdl_sys:
	cd src/rust-sdl2/sdl2-sys && cargo clean && cargo build --release
	cp src/rust-sdl2/sdl2-sys/target/release/libsdl2_sys-*.rlib obj/libsdl2_sys.rlib

_dynamic: _definitions obj
	$(RC) $(RFLAGS) src/dynamic/dynamic.rs -L obj --out-dir obj
	touch obj/.libdynamic.so.gate

_definitions:
	$(RC) $(RFLAGS) src/dynamic/definitions.rs --out-dir obj

obj:
	mkdir obj

clean:
	rm -r obj
	rm handmade_hero
